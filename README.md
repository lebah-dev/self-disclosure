# Sistem Identifikais Self Disclosure

## Technology Stack
- Lumen PHP Framework
- MySQL 5.7
- Bootstrap 4 Stisla UI

## Installation Setup
- Clone this repo or download latest release
- Copy `.env.example` to `.env`
- Update configuration on `.env` file
- Fill `APP_KEY` with random key for encryption
- Run command on terminal : `composer install` to install all dependencies
- Run command on terminal : `php artisan migrate` to migrate all tables
- Point your domain or subdomain to `./public` directory
