<?php

/** 
 * @var \Laravel\Lumen\Routing\Router $router 
 */

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\SelfDisclosureController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/login', [
    'as' => 'login',
    'uses' => UserController::class . '@login'
]);

$router->post('/login', [
    'as' => 'login.store',
    'uses' => UserController::class . '@store'
]);

$router->group(['middleware' => 'auth'], function ($router) {
    $router->get('/logout', [
        'as' => 'logout',
        'uses' => UserController::class . '@logout'
    ]);

    $router->get('/', [
        'as' => 'home',
        'uses' => HomeController::class . '@home'
    ]);
    
    $router->get('/materi', [
        'as' => 'materi',
        'uses' => HomeController::class . '@materi'
    ]);
    
    $router->get('/petunjuk', [
        'as' => 'petunjuk',
        'uses' => HomeController::class . '@petunjuk'
    ]);

    $router->get('/mulai', [
        'as' => 'mulai',
        'uses' => SelfDisclosureController::class . '@start'
    ]);

    $router->post('/mulai', [
        'as' => 'submit',
        'uses' => SelfDisclosureController::class . '@submit'
    ]);

    $router->get('/hasil', [
        'as' => 'hasil',
        'uses' => SelfDisclosureController::class . '@hasil'
    ]);
});
