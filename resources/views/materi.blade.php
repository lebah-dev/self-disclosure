<x-layout>
	<x-slot name="header">
		<h1><i class="fa fa-book small mr-2"></i> Materi</h1>
	</x-slot>

	<x-slot name="body">
		<div class="card">
			<div class="card-body">
				<h5 class="mb-4">Definisi Self Disclosure</h5>
				<p>Greene et al, dalam (Philipp K. Masur 2017) mendefinisikan pengungkapan diri sebagai interaksi antara setidaknya dua individu di mana seseorang bermaksud untuk dengan sengaja mengungkapkan sesuatu yang bersifat pribadi kepada orang lain, sejalan dengan itu, Jourard dan Lasakow dalam (Philipp K. Masur 2017) mengungkapkan bahwa pengungkapan diri merupakan suatu proses membuat diri berupa informasi dapat diketahui oleh orang lain, hal ini dapat berupa pembagian pengetahuan antara individu satu dengan lainnya.</p>
				<p>Menurut Kleinke dalam Devito (2011), kesadaran diri merupakan landasan bagi semua bentuk dan fungsi komunikasi. Landasan yang dimaksud dijelaskan dengan empat bentuk daerah atau kuadran berdasarkan teori Johari Window, yaitu antara lain :</p>
				<ol>
					<li>
						<h6>Daerah terbuka (Open Self)</h6>
						<p>Daerah terbuka Open Self yaitu berisikan semua informasi perilaku, sikap, perasaan, gagasan dan lain sebagainya yang diketahui oleh diri sendiri maupun oleh orang lain.</p>
						<p>Semakin kecil kuadran pertama ini, maka akan semakin buruk komunikasi, komunikasi bergantung pada sejauh mana kita membuka diri kepada orang lain dan kepada diri kita sendiri.</p>
					</li>
					<li>
						<h6>Daerah buta (Blind Self )</h6>
						<p>Daerah buta Blind Self yaitu berisikan informasi tentang diri kita yang diketahui oleh orang lain tetapi kita sendiri tidak mengetahuinya, ini dapat berupa hal hal kecil mengenai kebiasaan kita yang tidak kita sadari.</p>
						<p>Menurut Devito, akan sangat mustahil menghilangkan daerah buta, akan tetapi individu dapat memperkecil daerah buta ini. Peran pengungkapan diri dalam daerah buta ini adalah memberikan kesempatan kepada orang lain untuk memberikan informasi mengenai diri kita yang tidak kita ketahui.</p>
					</li>
					<li>
						<h6>Daerah gelap (Unknown Self)</h6>
						<p>Daerah gelap Unknown Self adalah bagian dari diri kita yang tidak diketahui baik diri kita maupun orang lain, ini merupakan informasi yang tenggelam didalam alam bawah sadar manusia atau sesuatu yang luput dari perhatian.</p>
						<p>Informasi yang terungkap didalam daerah ini cenderung berasal dari ketidak sadaran secara penuh, pengaruh obat, mimpi, hipnotis, atau melalui suatu tes proyektif.</p>
					</li>
					<li>
						<h6>Daerah tertutup (Hidden Self)</h6>
						<p>Daerah tertutup Hidden Self mengandung semua hal yang anda ketahui tentang diri sendiri dan orang lain namun dirahasiakan untuk diri sendiri.</p>
						<p>Pada bagian ini, orang yang menginformasikan segala hal tentang dirinya atau hidupnya maka disebut dengan overdisclosure. Sedangkan jika terlalu menutup diri yakni jarang sekali membicarakan tentang kehidupannya kepada orang lain maka disebut underdisclosure. Artinya pada kuadran ini individu memilih topik-topik mana yang akan diinformasikan dan dengan siapa dia akan mengungkapkannya.</p>
					</li>
				</ol>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<h5>Manfaat Self Disclosure</h5>
				<p>Menurut Devito (2011) keterbukaan diri memiliki berbagai macam manfaat bagi seseorang yaitu:</p>
				<ol>
					<li>Pengetahuan diri, manfaat dari keterbukaan diri salah satunya adalah individu mendapatkan pemahaman baru dan lebih mendalam mengenai dirinya sendiri. Dalam sebuah proses konseling misalnya, pandangan baru sering kali muncul pada diri konseli saat konseli melakukan pengungkapan diri. Konseli mungkin saja menyadari adanya aspek perilaku yang selama ini belum diketahuinya, oleh karena itu melalui keterbukaan diri individu dapat memahami dirinya secara  lebih mendalam.</li>
					<li>Kemampuan mengatasi kesulitan, salah satu perasan takut yang besar pada individu adalah ketakutan ketika tidak diterima dalam suatu lingkungan karena suatu kesalahan tertentu seperti kesalahan kepada orang lain. Keterbukaan diri akan membantu individu dalam menyelesaikan suatu permasalahan dengan orang lain karena individu memiliki kesiapan untuk membicarakan permasalahan tersebut secara lebih terbuka.</li>
					<li>Efisiensi komunikasi, keterbukaan diri yang dilakukan individu dapat mempengaruhi proses komunikasi yang dilakukannya. Individu dapat lebih memahami apa yang dikatakan oleh orang lain apabila individu tersebut sudah mengenal baik orang lain tersebut, sehingga individu tersebut mendapatkan pemahaman secara utuh terhadap orang lain dan mungkin sebaliknya. Sehingga proses komunikasi yang dilakukan menjadi tepat dan efektif.</li>
				</ol>
				<p>Kedalaman hubungan. Keterbukaan diri sangat diperlukan dalam membina suatu hubungan yang bermakna seperti sikap saling percaya, menghargai, dan jujur. Adanya keterbukaan akan membuat suatu hubungan lebih bermakna dan mendalam.</p>
			</div>
		</div>
	</x-slot>
</x-layout>