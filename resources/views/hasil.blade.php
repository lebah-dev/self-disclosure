<x-layout>
	<x-slot name="header">
		<h1><i class="fa fa-file-contract small mr-2"></i> Hasil</h1>
	</x-slot>

	<x-slot name="body">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12 my-4">
						<canvas class="mx-auto w-75" style="height: 250px;" id="result"></canvas>
						<div class="mt-4">
							<div>Note :</div>
							<div><span class="badge badge-warning">Close Area</span> <span class="badge badge-success">Open Area</span></div>
						</div>
						<div class="text-right">
							<button class="btn btn-dark" data-toggle="modal" data-target="#descriptionModal">Description</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</x-slot>

	<x-slot name="modals">
		<div class="modal fade" tabindex="-1" role="dialog" id="descriptionModal">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Description</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<ol class="px-3">
							<li>
								<h6>Daerah terbuka (Open Self)</h6>
								<p>Daerah terbuka Open Self yaitu berisikan semua informasi perilaku, sikap, perasaan, gagasan dan lain sebagainya yang diketahui oleh diri sendiri maupun oleh orang lain.</p>
								<p>Semakin kecil kuadran pertama ini, maka akan semakin buruk komunikasi, komunikasi bergantung pada sejauh mana kita membuka diri kepada orang lain dan kepada diri kita sendiri.</p>
							</li>
							<li>
								<h6>Daerah buta (Blind Self )</h6>
								<p>Daerah buta Blind Self yaitu berisikan informasi tentang diri kita yang diketahui oleh orang lain tetapi kita sendiri tidak mengetahuinya, ini dapat berupa hal hal kecil mengenai kebiasaan kita yang tidak kita sadari.</p>
								<p>Menurut Devito, akan sangat mustahil menghilangkan daerah buta, akan tetapi individu dapat memperkecil daerah buta ini. Peran pengungkapan diri dalam daerah buta ini adalah memberikan kesempatan kepada orang lain untuk memberikan informasi mengenai diri kita yang tidak kita ketahui.</p>
							</li>
							<li>
								<h6>Daerah gelap (Unknown Self)</h6>
								<p>Daerah gelap Unknown Self adalah bagian dari diri kita yang tidak diketahui baik diri kita maupun orang lain, ini merupakan informasi yang tenggelam didalam alam bawah sadar manusia atau sesuatu yang luput dari perhatian.</p>
								<p>Informasi yang terungkap didalam daerah ini cenderung berasal dari ketidak sadaran secara penuh, pengaruh obat, mimpi, hipnotis, atau melalui suatu tes proyektif.</p>
							</li>
							<li>
								<h6>Daerah tertutup (Hidden Self)</h6>
								<p>Daerah tertutup Hidden Self mengandung semua hal yang anda ketahui tentang diri sendiri dan orang lain namun dirahasiakan untuk diri sendiri.</p>
								<p>Pada bagian ini, orang yang menginformasikan segala hal tentang dirinya atau hidupnya maka disebut dengan overdisclosure. Sedangkan jika terlalu menutup diri yakni jarang sekali membicarakan tentang kehidupannya kepada orang lain maka disebut underdisclosure. Artinya pada kuadran ini individu memilih topik-topik mana yang akan diinformasikan dan dengan siapa dia akan mengungkapkannya.</p>
							</li>
						</ol>
					</div>
					<div class="modal-footer bg-whitesmoke br">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</x-slot>

	<x-slot name="scripts">
		<script src="{{ asset('assets/modules/chart.min.js') }}"></script>
		<script>
			var ctx = document.getElementById("result").getContext('2d');
			var data = @json($result);
			console.log(Object.keys(data))

			var datasets = (Object.keys(data)).map((item, index) => {
				return {
					label: data[item].category,
					data: [data[item].value],
					borderWidth: 2,
					backgroundColor: 'hsl(' + (index < 2 ? 50 : 130) + ', 90%, 60%)',
					borderColor: 'hsl(' + (index < 2 ? 50 : 130) + ', 90%, 60%)',
					borderWidth: 2.5,
					pointBackgroundColor: '#ffffff',
					pointRadius: 4
				}
			});

			var myChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ["Self Disclosure"],
					datasets: datasets
				},
				options: {
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							gridLines: {
								drawBorder: false,
								color: '#f2f2f2',
							},
							ticks: {
								beginAtZero: true,
								stepSize: 10
							}
						}],
						xAxes: [{
							ticks: {
								display: false
							},
							gridLines: {
								display: false
							}
						}]
					},
				}
			});
		</script>
	</x-slot>
</x-layout>