<x-layout>
	<x-slot name="header">
		<h1><i class="fa fa-book small mr-2"></i> Petunjuk</h1>
	</x-slot>

	<x-slot name="body">
		<div class="card">
			<div class="card-body">
				<h5 class="mb-4">Petunjuk Pengisian</h5>
				<ol>
					<li>Media ini merupakan alat untuk Identifikasi self disclosure individu berdasarkan empat dimennsi datau area berdasarkan teori Johari Window</li>
					<li>Mahasiswa membuka website </li>
					<li>Mengisi kolom username untuk masuk ke website</li>
					<li>Setelah login, individu diarahkan ke halaman yang berisi penjelasan tentang teori dan petunjuk pengisian</li>
					<li>Halaman pernyataan berisi tentang beberapa pernyataan untuk mengungkap self disclosure</li>
					<li>Individu mengisi empat pilihan setiap pernyataan yang disediakan</li>
					<li>Setelah pernyataan semua diisi, halaman selanjutnya akan menampilkan hasil berupa grafik yang menunjukan setiap dimensi atau area</li>
				</ol>
			</div>
		</div>
	</x-slot>
</x-layout>