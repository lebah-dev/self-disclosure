<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>Sistem Indentifikasi Self Disclosure</title>

	<!-- General CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

	<!-- CSS Libraries -->

	<!-- Template CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
</head>

<body class="layout-2">
	<div id="app">
		<div class="main-wrapper">
			<x-navbar></x-navbar>
			<x-sidebar></x-sidebar>

			<!-- Main Content -->
			<div class="main-content">
				<section class="section">
					<div class="section-header">
						{!! $header ?? '<h1>Sistem Identifikasi Self Disclosure</h1>' !!}
					</div>

					<div class="section-body">
						{{ $body ?? null }}
					</div>
				</section>
			</div>
			
			<x-footer></x-footer>
		</div>

		{{ $modals ?? null }}
	</div>

	<!-- General JS Scripts -->
	<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/modules/popper.js') }}"></script>
	<script src="{{ asset('assets/modules/tooltip.js') }}"></script>
	<script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
	<script src="{{ asset('assets/modules/moment.min.js') }}"></script>
	<script src="{{ asset('assets/js/stisla.js') }}"></script>

	<!-- JS Libraies -->
	<script src="{{ asset('assets/modules/sticky-kit.js') }}"></script>

	<!-- Page Specific JS File -->
	{{ $scripts ?? null }}

	<!-- Template JS File -->
	<script src="{{ asset('assets/js/scripts.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>