<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
	<a href="index.html" class="navbar-brand sidebar-gone-hide">Self Disclosure</a>
	<a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
	<div class="nav-collapse">
		<ul class="navbar-nav"></ul>
	</div>
	<form class="form-inline ml-auto">
		<ul class="navbar-nav">
		</ul>
	</form>
	<ul class="navbar-nav navbar-right">
		<li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
				<img alt="image" src="https://ui-avatars.com/api/?name={{ user()->username }}" class="rounded-circle mr-1">
				<div class="d-sm-none d-lg-inline-block">Hi, {{ user()->username }}</div>
			</a>
			<div class="dropdown-menu dropdown-menu-right">
				<a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger">
					<i class="fas fa-sign-out-alt"></i> Logout
				</a>
			</div>
		</li>
	</ul>
</nav>