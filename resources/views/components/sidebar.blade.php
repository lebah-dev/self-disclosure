<div class="main-sidebar">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand sidebar-gone-show"><a href="index.html">Self Disclosure</a></div>
		<ul class="sidebar-menu">
			<li class="menu-header">Menu</li>
			<li class="{{ activeRoute('home') ? 'active' : '' }}"><a class="nav-link" href="{{ route('home') }}"><i class="fa fa-home"></i> <span>Home</span></a></li>
			<li class="{{ activeRoute('materi') ? 'active' : '' }}"><a class="nav-link" href="{{ route('materi') }}"><i class="fa fa-book"></i> <span>Materi</span></a></li>
			<li class="{{ activeRoute('petunjuk') ? 'active' : '' }}"><a class="nav-link" href="{{ route('petunjuk') }}"><i class="fa fa-question"></i> <span>Petunjuk</span></a></li>
			<li class="{{ activeRoute('mulai') ? 'acive' : '' }}"><a class="nav-link text-primary" href="{{ route('mulai') }}"><i class="fa fa-play"></i> <span>Mulai</span></a></li>
		</ul>
	</aside>
</div>