<x-layout>
	<x-slot name="header">
		<h1><i class="fa fa-play small mr-2"></i> Mulai Identifikasi</h1>
	</x-slot>

	<x-slot name="body">
		<div class="card">
			<div class="card-body">
				<form action="{{ route('submit') }}" method="post">
					<input type="hidden" name="item" value="{{ $item->id }}">
					<input type="hidden" name="category" value="{{ $item->category }}">

					<h5 class="font-weight-normal">{{ $item->item }}</h5>
					<div class="mt-4">
						@foreach($item->choices as $choice)
						<label class="d-block mb-1"><input type="radio" name="value" value="{{ $choice['poin'] }}"> {{ $choice['text'] }}</label>
						@endforeach
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-primary btn-icon icon-left"><i class="fas fa-save"></i> Next Item</button>
					</div>
				</form>
			</div>
		</div>
	</x-slot>
</x-layout>