<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>Sistem Indentifikasi Self Disclosure</title>

	<!-- General CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

	<!-- CSS Libraries -->

	<!-- Template CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
</head>

<body>
  <img src="{{ asset('assets/img/bg.png') }}" alt="" class="position-fixed w-100 h-100" style="top: 0; left: 0;">
  <div id="app">
    <section class="section">
      <div class="container mt-3">
        <div class="row">
          <div class="d-none d-lg-block col-lg-7">
            <img class="my-5 py-5" src="{{ asset('assets/img/hero.png') }}" alt="">
          </div>
          <div class="col-12 col-lg-5">
            <div class="card">
              <div class="card-body">
                <div class="login-brand">
                  <div class="d-none d-lg-block mb-4">
                    <img style="width: 80px; height: 80px;" src="{{ asset('assets/img/logo-uty.png') }}" alt="UTY">
                    <img style="width: 80px; height: 80px;" src="{{ asset('assets/img/logo-konseling.png') }}" alt="Konseling">
                  </div>
                  <div class="d-block d-lg-none mb-4">
                    <img style="width: 60px; height: 60px;" src="{{ asset('assets/img/logo-uty.png') }}" alt="UTY">
                    <img style="width: 60px; height: 60px;" src="{{ asset('assets/img/logo-konseling.png') }}" alt="Konseling">
                  </div>
                  <h4 class="d-none d-lg-block">Sistem Identifikasi Self Disclosure</h4>
                  <h6 class="d-block d-lg-none">Sistem Identifikasi Self Disclosure</h6>
                </div>
    
                <div class="card card-primary">
                  <div class="card-body">
                    <h5 class="d-none d-lg-block mb-4">Selamat Datang,<br>di Web Identifikasi Self Disclosure</h5>
                    <h6 class="d-block d-lg-none mb-4">Selamat Datang,<br>di Web Identifikasi Self Disclosure</h6>
                    <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                      <p>Silahkan isi username anda untuk pendataan.</p>
    
                      <div class="form-group">
                        <label for="username">Username</label>
                        <input id="username" type="text" class="form-control" name="username" tabindex="1" required autofocus>
                        <div class="invalid-feedback">
                          Please fill in your username
                        </div>
                      </div>
    
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                          Login
                        </button>
                      </div>
                    </form>
    
                  </div>
                </div>
                <div class="my-0 simple-footer">
                  Copyright &copy; Self Disclosure 2021<br>Bimbingan Konseling Universitas Teknologi Yogyakarta
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- General JS Scripts -->
	<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/modules/popper.js') }}"></script>
	<script src="{{ asset('assets/modules/tooltip.js') }}"></script>
	<script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
	<script src="{{ asset('assets/modules/moment.min.js') }}"></script>
	<script src="{{ asset('assets/js/stisla.js') }}"></script>

	<!-- JS Libraies -->

	<!-- Page Specific JS File -->

	<!-- Template JS File -->
	<script src="{{ asset('assets/js/scripts.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>