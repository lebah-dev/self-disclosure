<x-layout>
	<x-slot name="header">
		<h1><i class="fa fa-home small mr-2"></i> Home</h1>
	</x-slot>

	<x-slot name="body">
		<div class="card">
			<div class="card-header">
				<h4>Self Disclosure</h4>
			</div>
			<div class="card-body">
				<img src="{{ asset('assets/img/home.png') }}" alt="" style="height: 200px;">
				<h6>Ketahui bagaiman pengungkapan diri anda</h6>
				<a href="{{ route('mulai') }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-play"></i> Mulai Identifikasi</a>
			</div>
		</div>
	</x-slot>
</x-layout>