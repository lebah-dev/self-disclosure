<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $fillable = [
		'username'
	];
	
	public function answers()
	{
		return $this->hasMany(Answer::class);
	}
}