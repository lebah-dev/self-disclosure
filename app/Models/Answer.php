<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Answer extends Model
{
	/**
	 * set fillable attribute for mass assignment
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'item', 'category', 'value'
	];

	/**
	 * 1 answer belongs to 1 one user
	 * @return BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}
}