<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class HomeController extends Controller
{
	/**
	 * Show home page
	 * 
	 * @return View
	 */
	public function home()
	{
		return view('home');
	}

	/**
	 * Show materi page
	 * 
	 * @return View
	 */
	public function materi()
	{
		return view('materi');
	}

	/**
	 * Show petunjuk page
	 * 
	 * @return View
	 */
	public function petunjuk()
	{
		return view('petunjuk');
	}
}