<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\User;
use App\Repositories\SelfDisclosureRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class SelfDisclosureController extends Controller
{
	/**
	 * @var SelfDisclosureRepository $repository
	 */
	private $repository;

	/**
	 * Class Constructor
	 */
	public function __construct(SelfDisclosureRepository $repository)
	{
		$this->repository = $repository;

	}

	/**
	 * Show start self disclosure identification page
	 * 
	 * @return View
	 */
	public function start(Request $request)
	{
		if ($request->item > $this->repository->itemCount()) {
			return redirect()->route('hasil');
		}

		$user = user();
		if (Answer::query()->where('user_id', $user->id)->count() >= $this->repository->itemCount()) {
			return redirect()->route('hasil');
		}

		return view('mulai', [
			'item' => $this->repository->getItems($request->item ?? 1)
		]);
	}

	/**
	 * Submit response
	 * 
	 * @param Request $request
	 * @return RedirectResponse
	 */
	public function submit(Request $request)
	{
		if (!$request->value) return redirect()->route('mulai', ['item' => $request->item]);

		try {
			DB::beginTransaction();

			// get user data
			$user = user();

			// update user's old answer if exist or insert new one
			Answer::query()
				->updateOrCreate([
					'user_id' => $user->id,
					'item' => $request->item,
					'category' => $request->category,
				], [
					'value' => $request->value
				]);

			DB::commit();
			return redirect()->route('mulai', ['item' => ($request->item + 1)]);
		} catch (\Exception $e) {
			DB::rollBack();
			dd($e);
		}
	}

	/**
	 * Calculate hasil
	 * 
	 * @return View
	 */
	public function hasil()
	{
		$user = user();
		$result = Answer::query()->where('user_id', $user->id)->get();
		$result = $result->groupBy('category')
		->transform(function (Collection $items, $index) {
			return [
				'category' => $this->repository->getCategory($index),
				'value' => round(($items->sum('value') / ($items->count() * 4)) * 100, 0)
			];
		});
		
		return view('hasil', [
			'result' => $result
		]);
	}
}