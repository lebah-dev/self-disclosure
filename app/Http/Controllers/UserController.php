<?php

namespace App\Http\Controllers;

use App\Models\User;
use Laravel\Lumen\Http\Request;

class UserController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function store()
    {
        if (request()->input('username')) {
            $user = User::query()->updateOrCreate([
                'username' => request()->input('username'),
            ]);

            request()->session()->put('username', $user->username);
        }

        return redirect()->route('home');
    }

    public function logout()
    {
        request()->session()->remove('username');
        return redirect()->route('login');
    }
}
