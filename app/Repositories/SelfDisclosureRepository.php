<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

class SelfDisclosureRepository
{
	/**
	 * @var array
	 */
	private $categories;

	/**
	 * @var array
	 */
	private $items;

	/**
	 * @var array
	 */
	private $choices;

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		$this->initCategories();
		$this->initChoises();
		$this->initItems();
	}

	/**
	 * Self Disclosure Categories
	 */
	private function initCategories()
	{
		$this->categories = [
			'Open Self',
			'Blind Self',
			'Unknown Self',
			'Hidden Self'
		];
	}

	private function initChoises()
	{
		$this->choices = [
			'Sangat setuju',
			'Setuju',
			'Tidak setuju',
			'Sangat tidak setuju',
		];
	}

	/**
	 * Item list.
	 * id		=> determent identity of item. it must be unique!
	 * item 	=> statement
	 * category => must in [1, 2, 3, 4] that stand for
	 * 			   [open-self, blind-self, unknown-self, hidden-self]
	 * type 	=> must in [-, +] that stand for
	 * 			   [negative choice, positive choice]
	 */
	private function initItems()
	{
		$this->items = [
			['id' => 1, 'category' => 1, 'type' => '+', 'item' => 'Saya dapat berinteraksi dengan orang lain baik kelompok atau individu dalam waktu yang lama'],
			['id' => 2, 'category' => 1, 'type' => '+', 'item' => 'Saya dapat mengungkapkan diri secara terbuka kepada siapapun'],
			['id' => 3, 'category' => 1, 'type' => '-', 'item' => 'Saya tidak nyaman untuk berinteraksi dalam waktu yang lama dengan orang lain'],
			['id' => 4, 'category' => 1, 'type' => '+', 'item' => 'Saya mengungkapkan diri secara terbuka hanya dengan orang tua'],
			['id' => 5, 'category' => 1, 'type' => '-', 'item' => 'Saya lebih tertutup ketika komunikasi secara kelompok'],
			['id' => 6, 'category' => 1, 'type' => '+', 'item' => 'Saya akan lebih terbuka jika komunikasi hanya dengan satu orang'],
	
			['id' => 7, 'category' => 2, 'type' => '+', 'item' => 'Saya lebih sering menceritakan pengalaman positif kepada orang lain'],
			['id' => 8, 'category' => 2, 'type' => '+', 'item' => 'Saya lebih sering menceritakan pengalaman negative kepada orang lain'],
			['id' => 9, 'category' => 2, 'type' => '-', 'item' => 'Saya tidak bisa menceritakan hal negative mengenai diri saya kepada orang lain'],
			['id' => 10, 'category' => 2, 'type' => '-', 'item' => 'Saya tidak bisa menceritakan hal positif kepada orang lain mengenai diri saya'],
			['id' => 11, 'category' => 2, 'type' => '+', 'item' => 'Saya memberikan saran kepada orang lain berdasarkan pengalaman'],
			['id' => 12, 'category' => 2, 'type' => '-', 'item' => 'Saya tidak ingin memberikan saran kepada orang lain, karena saya merasa kurang pengalaman'],
			['id' => 13, 'category' => 2, 'type' => '-', 'item' => 'Saya merasa tidak memiliki pengetahuan yang cukup untuk memotivasi orang lain'],
			['id' => 14, 'category' => 2, 'type' => '+', 'item' => 'Saya merasa mampu untuk memotivasi orang lain dengan pengetahuan yang saya miliki'],
	
			['id' => 15, 'category' => 3, 'type' => '+', 'item' => 'Saya mengungkapkan diri kepada teman dengan jujur'],
			['id' => 16, 'category' => 3, 'type' => '-', 'item' => 'Saya kurang jujur dalam mengungkapkan diri kepada orang lain'],
			['id' => 17, 'category' => 3, 'type' => '-', 'item' => 'Saya merasa belum terlalu paham mengenai diri saya sendiri untuk diungkapkan kepada orang lain'],
			['id' => 18, 'category' => 3, 'type' => '+', 'item' => 'Saya merasa apa yang saya ungkapkan ke orang lain mengenai diri saya sudah sesuai'],
			['id' => 19, 'category' => 3, 'type' => '+', 'item' => 'Saya selalu percaya diri ketika berbicara di depan umum'],
			['id' => 20, 'category' => 3, 'type' => '+', 'item' => 'Saya berusaha mengembangkan potensi yang saya miliki'],
			['id' => 21, 'category' => 3, 'type' => '-', 'item' => 'Saya tidak percaya diri untuk berbicara di depan umum'],
			['id' => 22, 'category' => 3, 'type' => '-', 'item' => 'Saya tidak memahami potensi apa yang dapat saya kembangkan'],
	
			['id' => 23, 'category' => 4, 'type' => '+', 'item' => 'saya ingin orang lain paham mengenai diri saya'],
			['id' => 24, 'category' => 4, 'type' => '-', 'item' => 'Saya tidak ingin orang lain tau mengenai diri saya'],
			['id' => 25, 'category' => 4, 'type' => '+', 'item' => 'Saya ingin lebih dekat dan akrab dengan semua oranng'],
			['id' => 26, 'category' => 4, 'type' => '-', 'item' => 'Saya tidak ingin dekat dan akrab dengan orang lain'],
			['id' => 27, 'category' => 4, 'type' => '+', 'item' => 'Sebelum mengungkapkan diri, saya akan memikirkan maksud dan tujuan saya dalam mengungkapkan sesuatu hal'],
			['id' => 28, 'category' => 4, 'type' => '-', 'item' => 'Saya cenderung mengungkapkan diri secara spontan kepada orang lain'],
			['id' => 29, 'category' => 4, 'type' => '+', 'item' => 'Saya mengontrol informasi yang akan disampaikan mengenai diri saya kepada orang lain'],
			['id' => 30, 'category' => 4, 'type' => '-', 'item' => 'Saya tidak bisa mengontrol informasi yang akan saya sampaikan mengenai diri saya kepada orang lain'],
		];
	}

	/**
	 * Get items count
	 * 
	 * @return int
	 */
	public function itemCount()
	{
		return count($this->items);
	}

	/**
	 * Get all available items
	 * 
	 * @return Collection
	 */
	public function getItems($index)
	{
		return collect(json_decode(json_encode($this->items)))
			->transform(function ($item) {
				$item->choices = $this->getChoises($item->type);
				return $item;
			})[$index-1];
	}

	/**
	 * Get item choises based on type
	 * 
	 * @param string $type
	 * @return Collection
	 */
	public function getChoises(string $type)
	{
		if ($type === '-') {
			return collect($this->choices)->transform(function ($item, $index) {
				return [
					'poin' => $index + 1,
					'text' => $item
				];
			});
		} else {
			return collect($this->choices)->transform(function ($item, $index) {
				return [
					'poin' => (($index + 1) * -1) + 5,
					'text' => $item
				];
			});
		}
	}

	/**
	 * Get category by id
	 * 
	 * @param int $index
	 * @return string
	 */
	public function getCategory(int $index)
	{
		return $this->categories[$index-1] ?? '';
	}
}