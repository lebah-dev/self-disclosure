<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Routing\UrlGenerator;

if (!function_exists('asset')) {
	/**
	 * @param string $path
	 * @param bool $secure
	 * @return string
	 */
	function asset(string $path, bool $secure = false)
	{
		return (new UrlGenerator(app()))->asset($path, $secure);
	}
}

if (!function_exists('activeRoute')) {
	/**
	 * @param string $route
	 * @return bool
	 */
	function activeRoute(string $route)
	{
		return request()->routeIs($route);
	}
}

if (!function_exists('user')) {
	/**
	 * @return User|null
	 */
	function user()
	{
		return User::query()->where('username', request()->session()->get('username'))->first();
	}
}